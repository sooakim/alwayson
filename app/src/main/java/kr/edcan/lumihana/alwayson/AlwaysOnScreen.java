package kr.edcan.lumihana.alwayson;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

public class AlwaysOnScreen extends AppCompatActivity {
    PowerManager.WakeLock wakeLock;
    BroadcastReceiver mPowerBroadcast;
    IntentFilter powerFilter;
    TextView screen_text_title;
    SharedPreferences sharedPreferences;
    String titleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_always_on_screen);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        screen_text_title = (TextView) findViewById(R.id.screen_text_title);
        sharedPreferences = getSharedPreferences("Text", MODE_PRIVATE);
        titleText = sharedPreferences.getString("title", null);

        if(titleText != null){
            screen_text_title.setText(titleText);
        }

        acquireWakeLock(AlwaysOnScreen.this);

        if (powerFilter == null) powerFilter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        if (mPowerBroadcast == null) {
            mPowerBroadcast = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (intent.getAction()) {
                        case "android.intent.action.SCREEN_OFF": {
                            finish();
                        }

                        case "android.intent.action.SCREEN_ON": {
                            break;
                        }

                        default:
                            return;
                    }
                }
            };
        }
        registerReceiver(mPowerBroadcast, powerFilter);

        Log.e("ok", "launched");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseWakeLock();
        unregisterReceiver(mPowerBroadcast);
    }

    private void acquireWakeLock(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP,
                context.getClass().getName());

        if(wakeLock != null){
            wakeLock.acquire();
        }
    }

    private void releaseWakeLock(){
        if(wakeLock!=null){
            wakeLock.release();
            wakeLock=null;
        }
    }
}
