package kr.edcan.lumihana.alwayson;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by kimok_000 on 2016-04-21.
 */
public class AlwaysOnService extends Service {
    IntentFilter powerFilter;
    BroadcastReceiver mPowerBroadcast;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if(mPowerBroadcast==null){
            mPowerBroadcast = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (intent.getAction()){
                        case "android.intent.action.SCREEN_OFF" : {
                            Log.e("ok", "sens");
                            Intent intent_activity = new Intent(AlwaysOnService.this, AlwaysOnScreen.class);
                            intent_activity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent_activity);
                            Log.e("ok", "sense");
                            break;
                        }

                        case "android.intent.action.SCREEN_ON" : {
                            break;
                        }

                        default : return;
                    }
                }
            };
        }
        if(powerFilter==null) powerFilter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mPowerBroadcast, powerFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mPowerBroadcast);
    }
}
