package kr.edcan.lumihana.alwayson;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button main_button_start, main_button_stop;
    EditText main_edit_title;
    Intent main_intent_service;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String titleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("Text", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        main_button_start = (Button) findViewById(R.id.main_button_start);
        main_button_stop = (Button) findViewById(R.id.main_button_stop);
        main_edit_title = (EditText) findViewById(R.id.main_edit_title);

        titleText = sharedPreferences.getString("title",null);

        if(titleText!=null){
            main_edit_title.setText(titleText);
        }

        main_edit_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editor.putString("title", s.toString().trim());
                editor.commit();
            }
        });

        main_button_start.setOnClickListener(this);
        main_button_stop.setOnClickListener(this);
        main_intent_service = new Intent(getApplicationContext(),AlwaysOnService.class);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.main_button_start : {
                startService(main_intent_service);
                break;
            }
            case R.id.main_button_stop : {
                stopService(main_intent_service);
                break;
            }
        }
    }
}
